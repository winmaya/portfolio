import React, { Component } from 'react';

import './Counter.css';

class Counter extends Component {
	state = {
		counter:0,
	}
	handleClick =()=> {
		this.setState ({
			counter:this.state.counter +1,
		})
	}
	handleChange =()=> {
		this.setState ({
			counter:this.state.counter -1,
		})
	}

  	render() {
	    return (
		    <div className="container">
		    	 <div className="counter"> 
			      	<Plus doPlus={this.handleClick}/>
			      	<Minus doMinus={this.handleChange}/>			     
			      	<Count counter={this.state.counter}/>
				 </div>
		    </div>
	    );
  	}
}

class Plus extends Component {
	render(){
		return(
			<div>
				<button className="button" onClick={this.props.doPlus}>
					wanna + 1
				</button>
			</div>
		)
	}
}
class Minus extends Component {
	render(){
		return(
			<div>
				<button className="button" onClick={this.props.doMinus}>
					wanna - 1
				</button>
			</div>
		)
	}
}

class Count extends Component {

	render() {
		return(
			<div>
				<div className="result">
					= {this.props.counter}
				</div>
			</div>
		)
	}
}
export default Counter;
