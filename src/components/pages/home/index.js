import React, { Component } from 'react';


import './Home.css';

class Home extends Component {
  render() {
    return (
      <div className="Home">
      	<header className="header">
	      		<ul><span className="lucylogo">LUCY AHN </span>
		        	<li className="menu-home">Home</li>
		        	<li className="menu-portfolio">Portfolio</li>
		        	<li className="menu-interest">Interest</li>
		        	<li className="menu-blog">Blog</li>
		        	<li className="menu-practice">React</li>
	        	</ul>

        </header>
      </div>
    );
  }
}

export default Home;
