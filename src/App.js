import React, { Component } from 'react';
import Home from './components/pages/home';
import Counter from './components/pages/counter';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Home />
        <Counter />
      </div>
    );
  }
}



export default App;
